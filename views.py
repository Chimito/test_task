from tornado.web import RequestHandler
from tornado.escape import json_decode
from utils import create_access_key, update_statistic
from typing import Awaitable, Optional

repository = {}
statistic = {}

error_descriptions = {'not found': 'Does not exist',
                      'not body': 'Request body must contains even one pair: '
                                  'key + value',
                      'not key': 'Request must contains query param: key'}


class GetDataHandler(RequestHandler):
    async def get(self):
        key = self.get_query_argument('key', None)
        if key is None:
            self.set_status(status_code=400)
            self.write({'detail': error_descriptions.get('not key')})
            return

        resp_data = repository.get(key)
        if resp_data is None:
            self.set_status(status_code=404)
            self.write({'detail': error_descriptions.get('not found')})
            return

        resp_data.update({'duplicates': statistic.get(key, 0) - 1})
        self.write(resp_data)


class CreateDataHandler(RequestHandler):
    async def post(self):
        if not self.json:
            self.set_status(400)
            self.write({'detail': error_descriptions.get('not body')})
            return

        access_key = create_access_key(self.json)
        repository.update({access_key: self.json})
        update_statistic(access_key, statistic)

        self.set_status(status_code=201)
        self.write({'key': access_key})

    async def prepare(self) -> Optional[Awaitable[None]]:
        self.json = json_decode(self.request.body)


class UpdateDataHandler(RequestHandler):
    async def put(self):
        key = self.get_query_argument('key', None)
        if key is None:
            self.set_status(status_code=400)
            self.write({'detail': error_descriptions.get('not key')})
            return

        if key not in repository:
            self.set_status(status_code=404)
            self.write({'detail': error_descriptions.get('not found')})
            return

        if not self.json:
            self.set_status(400)
            self.write({'detail': error_descriptions.get('not body')})
            return

        repository.pop(key)
        statistic.pop(key)
        access_key = create_access_key(self.json)
        repository.update({access_key: self.json})
        update_statistic(access_key, statistic)

        self.write({'key': access_key})

    async def prepare(self) -> Optional[Awaitable[None]]:
        self.json = json_decode(self.request.body)


class DeleteDataHandler(RequestHandler):
    async def delete(self):
        key = self.get_query_argument('key', None)
        if key is None:
            self.set_status(status_code=400)
            self.write({'detail': error_descriptions.get('not key')})
            return

        if key not in repository:
            self.set_status(status_code=404)
            self.write({'detail': error_descriptions.get('not found')})
            return

        repository.pop(key)
        statistic.pop(key)

        self.set_status(status_code=204)


class StatisticHandler(RequestHandler):
    def get(self):
        original_counter = len(repository)
        if original_counter:
            double_counters_sum = sum(statistic.values()) - len(statistic)
            resp_data = {'statistic': double_counters_sum /
                         original_counter * 100}
            self.write(resp_data)
            return

        self.write({'statistic': 0})
