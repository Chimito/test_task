import os
import tornado.web
from tornado import ioloop
from views import CreateDataHandler, GetDataHandler, DeleteDataHandler, \
    UpdateDataHandler, StatisticHandler

SERVER_PORT = os.getenv('SERVER_PORT', 8089)


def make_app():
    return tornado.web.Application([
        (r'/api/add', CreateDataHandler),
        (r'/api/get', GetDataHandler),
        (r'/api/remove', DeleteDataHandler),
        (r'/api/update', UpdateDataHandler),
        (r'/api/statistic', StatisticHandler)
    ])


app = make_app()
app.listen(SERVER_PORT)
ioloop.IOLoop.current().start()
