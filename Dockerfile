FROM python:3

WORKDIR /app

COPY . .

RUN python3 -m pip install pipenv && pipenv install --system