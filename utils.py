import base64
from typing import Dict, Any


def create_access_key(data: Dict[str, Any]) -> str:
    access_key = b''
    for key, value in data.items():
        key_body = key + str(value)
        access_key += base64.encodebytes(bytes(key_body, 'utf-8')).strip()
    access_key = access_key.decode('utf-8')
    return access_key


def update_statistic(access_key: str, statistic_storage: Dict[str, int]) -> None:
    statistic_counter = statistic_storage.get(access_key, 0)
    statistic_storage.update({access_key: statistic_counter + 1})
